# MaterialDesignLearning
Hi guys, today I provided some source-codes to help you use the material components in your projects.
I hope this is useful for you.

### Screenshots
![ezgif com-gif-maker](https://user-images.githubusercontent.com/52744015/102204459-225bc200-3edf-11eb-8ab5-13d814c33a62.png)
![ezgif com-gif-maker (1)](https://user-images.githubusercontent.com/52744015/102204451-212a9500-3edf-11eb-9e7b-6fee819b6268.png)


![ezgif com-gif-maker (2)](https://user-images.githubusercontent.com/52744015/102204446-2091fe80-3edf-11eb-8b68-12c4ec7dfe7f.png)
![ezgif com-gif-maker (3)](https://user-images.githubusercontent.com/52744015/102204436-1ec83b00-3edf-11eb-89e6-3523459c177a.png)
